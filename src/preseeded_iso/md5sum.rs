use std::fs::{File, write};
use std::io::{Read, Write};
use std::os::unix::fs::PermissionsExt;
use std::path::Path;
use std::process::{Command, self, Stdio};

pub(crate) fn regenerate_md5sum(directory: &Path) {
    let md5sum_file_buf = directory.join("md5sum.txt");
    let md5sum_path = md5sum_file_buf.as_path();
    let md5sum_file = File::open(md5sum_path).unwrap();
    let md5sum_metadata = md5sum_file.metadata().unwrap();
    let mut md5sum_permissions = md5sum_metadata.permissions();

    // make md5sum file writable
    md5sum_permissions.set_mode(0o644);
    md5sum_file.set_permissions(md5sum_permissions.clone()).expect("failed to make md5sum.txt writeable");
    md5sum_file.sync_all().expect("failed to sync md5sum.txt write permissions to disk");

    // index installer files
    let find_process = Command::new("find")
	.args(["-follow", "-type", "f", "!", "-name", "md5sum.txt", "-print0"])
	.stdout(Stdio::piped())
	.stderr(Stdio::piped())
	.current_dir(directory)
	.spawn()
	.expect("failed to spawn find process");
    let find_output = find_process.wait_with_output().expect("failed to execute find process"); 
    let expected_error = "find: File system loop detected; ‘./debian’ is part of the same file system loop as ‘.’.\n";
    if !find_output.status.success() {
	match expected_error == String::from_utf8(find_output.stderr).unwrap().as_str() {
	    true => (),
	    false => {
		println!("find failed to index the installer files");
		process::exit(1);
	    }
	}
    }

    // regenerate message digest
    let mut xargs_process = Command::new("xargs")
	.args(["-0", "md5sum"])
	.stdin(Stdio::piped())
	.stdout(Stdio::piped())
	.current_dir(directory)
	.spawn()
	.expect("failed to spawn xargs process");
    let mut xargs_stdin = xargs_process.stdin.take().expect("failed to open xargs process stdin");
    xargs_stdin.write_all(&find_output.stdout).expect("failed to write find results to xargs process stdin");
    std::mem::drop(xargs_stdin);
    let mut xargs_stdout = xargs_process.stdout.take().expect("failed to open xargs process stdout");
    let mut xargs_output: Vec<u8> = vec![];
    xargs_stdout.read_to_end(&mut xargs_output).expect("failed to read xargs process stdout");
    let xargs_status = xargs_process.wait().expect("failed to execute xargs process");
    if !xargs_status.success() {

	println!("xargs/md5sum failed to regenerate and update the message digest file");
	process::exit(1);
    }
    write(md5sum_path, xargs_output).expect("failed to write recalculated message digest to md5sum.txt");
    
    // make md5sum file read-only
    md5sum_permissions.set_mode(0o644);
    md5sum_file.set_permissions(md5sum_permissions).expect("failed to make md5sum.txt read-only");
    md5sum_file.sync_all().expect("failed to sync md5sum.txt read-only permissions to disk");

    // null return
    return ();
}
