use std::path::Path;
use std::process::{Command, self};

pub(crate) fn extract_mbr_template(installer_iso: &Path, destination: &Path) {
    let installer_file_str = installer_iso.to_str().unwrap();
    let output_file_str = destination.to_str().unwrap();
    let dd_status = Command::new("dd")
	.arg(format!("if={installer_file_str}").as_str())
	.arg(format!("of={output_file_str}").as_str())
	.args(["bs=1", "count=432", "status=progress"])
	.status()
	.expect("failed to execute dd");
    if !dd_status.success() {
	println!("dd failed to extract the MBR template image from the installer image");
	process::exit(1);
    }

    // null return
    return ();
}
