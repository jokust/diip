use std::fs::{copy, File, remove_file};
use std::io::Write;
use std::os::unix::fs::PermissionsExt;
use std::path::Path;
use std::process::{Command, self, Stdio};

pub(crate) fn append_preseed_to_initrdgz(initrd_parent_directory: &Path, preseed: &Path) {
    let initrd_path_buf = &initrd_parent_directory.join("initrd");
    let initrd_path = initrd_path_buf.as_path();
    let initrdgz_path_buf = &initrd_parent_directory.join("initrd.gz");
    let initrdgz_path = initrdgz_path_buf.as_path();
    let initrdgz_file = File::open(initrdgz_path).unwrap();
    let initrdgz_metadata = initrdgz_file.metadata().unwrap();
    let mut initrdgz_permissions = initrdgz_metadata.permissions();
    let parent_directory_file = File::open(initrd_parent_directory).unwrap();
    let parent_directory_metadata = initrd_parent_directory.metadata().unwrap();
    let mut parent_directory_permissions = parent_directory_metadata.permissions();

    // make parent directory writeable
    parent_directory_permissions.set_mode(0o755);
    parent_directory_file.set_permissions(parent_directory_permissions.clone()).expect("failed to make initrd parent directory writeable");
    parent_directory_file.sync_all().expect("failed to sync initrd parent directory write permission to disk");
    // make initrdgz writable
    initrdgz_permissions.set_mode(0o644);
    initrdgz_file.set_permissions(initrdgz_permissions.clone()).expect("failed to make initrd.gz writeable");
    initrdgz_file.sync_all().expect("failed to sync initrd.gz write permission to disk");

    // decompress initrdgz to initrd
    let gunzip_status = Command::new("gunzip")
	.args(["--verbose", initrdgz_path.to_str().unwrap()])
	.status()
	.expect("failed to execute gunzip process");
    if !gunzip_status.success() {
	println!("gunzip failed to decompress initrd.gz");
	process::exit(1);
    }

    // append preseed contents to initrd
    let preseed_parent_tempdir = tempfile::tempdir().unwrap();
    let preseed_tempfile_buf = preseed_parent_tempdir.path().join("preseed.cfg");
    let preseed_tempfile = preseed_tempfile_buf.as_path();
    copy(preseed, preseed_tempfile).unwrap();
    let mut cpio_process = Command::new("cpio")
	.args(["--create", "--file", initrd_path.to_str().unwrap(), "--format", "newc", "--verbose", "--append",])
	.stdin(Stdio::piped())
	.stdout(Stdio::piped())
	.current_dir(preseed_parent_tempdir.path())
	.spawn()
	.expect("failed to spawn cpio child process");
    let mut cpio_stdin = cpio_process.stdin.take().expect("failed to open cpio child process stdin");
    cpio_stdin.write_all("preseed.cfg\n".as_bytes())
	.expect("failed to write index of the preseed file to the cpio child process");
    // must manually drop cpio_stdin for cpio to terminate
    std::mem::drop(cpio_stdin);
    let cpio_status = match cpio_process.try_wait().expect("failed to execute cpio child process") {
	Some(s) => s,
	None => cpio_process.wait().expect("failed to obtain cpio process exit status"),
    };
    if !cpio_status.success() {
	println!("cpio failed to append the preseed file contents to the initrd file");
	process::exit(1);
    }

    // clean temporary files
    remove_file(preseed_tempfile).expect("failed to delete temporary copy of preseed file");
    preseed_parent_tempdir.close().unwrap();

    // recompress initrd to initrdgz
    let gzip_status = Command::new("gzip")
	.args(["--verbose", initrd_path.to_str().unwrap()])
	.status()
	.expect("failed to execute gzip");
    if !gzip_status.success() {
	println!("gzip failed to recompress initrd");
	process::exit(1);
    }

    // make initrdgz read-only
    initrdgz_permissions.set_mode(0o444);
    initrdgz_file.set_permissions(initrdgz_permissions).expect("failed to make initrd.gz read-only");
    initrdgz_file.sync_all().expect("failed to sync initrd.gz read-only permission to disk");
    // make parent directory read-only
    parent_directory_permissions.set_mode(0o555);
    parent_directory_file.set_permissions(parent_directory_permissions).expect("failed to make initrd parent directory read-only");
    parent_directory_file.sync_all().expect("failed to sync initrd parent directory read-only permission to disk");

    // null return
    return ();
}
