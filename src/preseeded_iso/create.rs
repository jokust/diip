use std::path::Path;

use crate::force_clean::force_clean;
use crate::preseeded_iso::initrdgz::append_preseed_to_initrdgz;
use crate::preseeded_iso::installer_iso::{
    extract_filesystem_from_installer_iso, repack_bootable_iso,
};
use crate::preseeded_iso::mbr_template::extract_mbr_template;
use crate::preseeded_iso::md5sum::regenerate_md5sum;
use crate::preseeded_iso::PreseededISO;

impl PreseededISO {
    pub fn create_from_installers_and_preseed_files(
        installers: String,
        preseed: String,
    ) -> Result<Self, String> {
        let workdir = tempfile::tempdir().unwrap();
        let install_files_dir = workdir.path().join("install.amd");
        let outdir = std::env::current_dir().unwrap();
        let installer_file = Path::new(&installer);

        extract_filesystem_from_installer_iso(workdir.path(), installer_file);

        let preseed_file = Path::new(&preseed);
        append_preseed_to_initrdgz(install_files_dir.as_path(), preseed_file);

        regenerate_md5sum(workdir.path());

        let mbr_template_tempfile = tempfile::NamedTempFile::new().unwrap();
        extract_mbr_template(installer_file, mbr_template_tempfile.path());

        let mut installer_file_name = installer_file.file_name().unwrap().to_str().unwrap();
        installer_file_name = match installer_file_name.strip_suffix(".iso") {
            Some(s) => s,
            None => installer_file_name,
        };
        let installer_file_name_str = installer_file_name.replace("-", " ");
        installer_file_name = installer_file_name_str.as_str();
        let installer_source_volid_first_owned: String =
            installer_file_name.get(0..1).unwrap().to_uppercase();
        let installer_source_volid_first: &str = installer_source_volid_first_owned.as_str();
        let installer_source_volid_last: &str = installer_file_name.get(1..).unwrap();
        let installer_source_volid_owned: String =
            format!("{installer_source_volid_first}{installer_source_volid_last}");
        let installer_source_volid: &str = installer_source_volid_owned.as_str();
        println!("using source volume ID from file name: '{installer_source_volid}'");
        let mut volid_owned: String = format!("Auto {installer_source_volid}");
        let mut file_name_owned = volid_owned.clone();
        volid_owned.truncate(30);
        let volid: &str = volid_owned.as_str();
        file_name_owned = file_name_owned.to_lowercase();
        let mut file_name_string = file_name_owned.as_str().replace(" ", "-");
        file_name_string.push_str(".iso");
        let file_name = file_name_string.as_str();
        let file_path = Path::new(file_name);
        let absolute_file_path = outdir.join(file_path);

        repack_bootable_iso(
            &absolute_file_path,
            &workdir.path(),
            mbr_template_tempfile.path(),
            volid,
        );

        // clean mbr template tempfile
        mbr_template_tempfile
            .close()
            .expect("failed to remove MBR template temporary file");

        // clean working directory
        force_clean(workdir.path());
        workdir
            .close()
            .expect("failed to remove installer files temporary directory");

        // instantiate class using path of created bootable ISO
        let inst = Self::new(absolute_file_path);
        return Ok(inst);
    }
}
