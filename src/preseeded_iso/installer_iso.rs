use std::path::Path;
use std::process::{Command, self};

pub(crate) fn extract_filesystem_from_installer_iso(working_directory: &Path, installer_iso: &Path) {
    let xorriso_status = Command::new("xorriso")
	.args(["-osirrox", "on", "-indev", installer_iso.to_str().unwrap(), "-extract", "/", working_directory.to_str().unwrap()])
	.status()
	.expect("failed to execute xorriso");
    if !xorriso_status.success() {
	println!("xorriso failed to extract the ISO file system");
	process::exit(1);
    }

    // null return
    return ();
}

/// pack a directory of installer files into a bootable installer ISO
pub(crate) fn repack_bootable_iso(destination: &Path, iso_files: &Path, isohybrid_mbr_template: &Path, volume_id: &str) {
    match validate_volid(volume_id) {
	Ok(o) => o,
	Err(e) => panic!("{e}"),
    };
    // TODO: support excluding the grub efi image for legacy Debians (6)
    let xorriso_status = Command::new("xorriso")
	.args(["-as", "mkisofs", "-r", "-V", volume_id, "-o", destination.to_str().unwrap(), "-J", "-J", "-joliet-long"])
	.args(["-cache-inodes", "-isohybrid-mbr", isohybrid_mbr_template.to_str().unwrap(), "-b", "isolinux/isolinux.bin"])
	.args(["-c", "isolinux/boot.cat", "-boot-load-size", "4", "-boot-info-table", "-no-emul-boot", "-eltorito-alt-boot"])
	.args(["-e", "boot/grub/efi.img", "-no-emul-boot", "-isohybrid-gpt-basdat", "-isohybrid-apm-hfsplus"])
	.arg(iso_files.to_str().unwrap())
	.status()
	.expect("failed to execute xorriso");
    if !xorriso_status.success() {
	println!("xorriso failed to repack the installer files into a bootable ISO image");
	process::exit(1);
    }

    // null return
    return ();
}

/// validate the supplied string is a valid volid for xorriso according to our implementation of validity
///
/// # Volume ID rationale
/// The
/// [xorriso -volid option manpage](https://manpages.debian.org/bookworm/xorriso/xorriso.1.en.html#volid)
/// specifies that the volid (-V) option has space for 32 characters,
/// but according to rarely obeyed specs stricter rules apply:
/// - ECMA-119 demands ASCII characters out of [A-Z0-9_] like "IMAGE_23"
/// - Joliet allows 16 UCS-2 characters like "Windows name"  
/// I am implementing this as 32 or less of [a-zA-Z0-9 _-.] like:
///
/// ```
/// "Auto Debian 9.3.0 amd64 netinst"
/// ```
fn validate_volid(volume_id: &str) -> Result<&str, String> {
    let invalid_characters = regex::Regex::new(r"[^a-zA-Z0-9 _\-\.]").unwrap();

    return match invalid_characters.is_match(volume_id) {
	true => {
	    Err(format!("invalid volume ID: '{volume_id}'"))
	},
	false => Ok(volume_id),
    };
}
