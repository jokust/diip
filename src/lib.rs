#![doc = include_str!("../README.md")]

extern crate regex;
extern crate tempfile;

pub mod cli;
mod force_clean;
pub mod preseeded_iso;
