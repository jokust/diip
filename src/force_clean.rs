use std::fs::{File, read_dir, remove_dir, remove_file};
use std::path::Path;

/// in the given directory, recursively set all files and directories as writable
/// and delete them without deleting the parameter directory
pub(crate) fn force_clean(dir: &Path) {
    let contents = read_dir(dir).expect("failed to enumerate contents of directory '{dir}'");
    for item in contents {
	let dir_entry = item.unwrap();
	let dir_entry_pathbuf = dir_entry.path();
	let dir_entry_path = dir_entry_pathbuf.as_path();
	let dir_entry_path_str = dir_entry_path.to_str().unwrap();
	let file_type = dir_entry.file_type().unwrap();

	// item is type symlink
	if file_type.is_symlink() {
	    // delete item
	    remove_file(dir_entry_path).expect(format!("failed to delete file '{dir_entry_path_str}'").as_str());
	    continue;
	}

	let file = File::open(dir_entry_path)
	    .expect(format!("unable to open file '{dir_entry_path_str}' for removal").as_str());
	let file_metadata = file.metadata().unwrap();
	let mut file_permissions = file_metadata.permissions();

	// set write permissions
	file_permissions.set_readonly(false);
	file.set_permissions(file_permissions)
	    .expect(format!("failed to set write permissions for file '{dir_entry_path_str}'").as_str());
	// file.sync_all().expect(format!("failed to sync write permissions to disk for file '{dir_entry_path_str}'").as_str());

	// item is type directory
	if file_type.is_dir() {
	    // recurse to remove contents
	    force_clean(dir_entry_path);

	    // delete item
	    remove_dir(dir_entry_path).expect(format!("failed to delete directory '{dir_entry_path_str}'").as_str());
	    continue;
	}

	// item is type file
	if file_type.is_file() {
	    // delete item
	    remove_file(dir_entry_path).expect(format!("failed to delete file '{dir_entry_path_str}'").as_str());
	    continue;
	}
    }

    // null return
    return ();
}

