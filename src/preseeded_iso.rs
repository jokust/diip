use std::path::PathBuf;

pub struct PreseededISO {
    file_path: PathBuf,
}

impl PreseededISO {
    pub fn new(file_path: PathBuf) -> Self {
	Self { file_path }
    }

    pub fn print(&self) {
        let f = &self.file_path.display();

        println!("{f}");
    }
}

mod create;
mod initrdgz;
mod installer_iso;
mod mbr_template;
mod md5sum;
