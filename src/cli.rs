// use crate::preseeded_iso::PreseededISO;
use clap::{Parser, Subcommand};

use crate::preseeded_iso::PreseededISO;

#[derive(Parser)]
#[command(version, about, long_about = None)]
pub struct ArgumentParser {
    #[command(subcommand)]
    action: Actions,
}

#[derive(Subcommand)]
enum Actions {
    /// Add a preseed file to an installer image file
    Add {
        #[arg(short, long)]
        installers: String,
        #[arg(short, long)]
        preseed: String,
    },
    /// Create preseeded installer file
    Create {
        #[arg(short, long)]
        debian_versions: Vec<u8>, // wouldn't it be funny if we got to Debian 256
        #[arg(short, long)]
        preseed: String,
    },
    /// Output the contents of the preseed file from an installer image
    Read {
        #[arg(short, long)]
        installer: String,
    },
}

pub fn cli_run(args: Vec<String>) -> Result<(), String> {
    match ArgumentParser::try_parse_from(args) {
        Ok(cli_args) => match act_on(cli_args) {
            Ok(_t) => Ok(()),
            Err(action_error) => Err(action_error),
        },
        Err(parse_error) => Err(parse_error.to_string()),
    }
}

fn act_on(cli_args: ArgumentParser) -> Result<(), String> {
    let cli_action_result = match cli_args.action {
        Actions::Add { installers, preseed } => {
            PreseededISO::create_from_installers_and_preseed_files(installers, preseed)
        },
	Actions::Create { debian_versions, preseed } => {
	    PreseededISO::create_from_versions_and_preseed_file(debian_versions, preseed)
	},
        Actions::Read { installer } => {
	    PreseededISO::read_preseed_from_installer_file(installer)
	}
    };
    match cli_action_result {
	Ok(_t) => Ok(()),
	Err(e) => Err(e),
    }
}
