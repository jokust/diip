use diip::cli::cli_run;
use std::env;
use std::process::ExitCode;

// fn main() -> Result<(), String> {
fn main() -> ExitCode {
    let args: Vec<String> = env::args().collect();
    match cli_run(args) {
        Ok(_t) => ExitCode::SUCCESS,
        Err(e) => {
            println!("{e}");
            ExitCode::FAILURE
        }
    }
}
